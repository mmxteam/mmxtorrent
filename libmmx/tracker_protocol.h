#include "utilities.h"
#include <inttypes.h>

typedef struct MMXTConnect {
	UINT64 connection_id;
	UINT32 action;
	UINT32 transaction_id;
} mxTConnect;

typedef struct MMXTAnnounce {
	int64_t	connection_id;	// The connection id acquired from establishing the connection. 
	int32_t	action;	//Action.in this case, 1 for announce.See actions.
	int32_t	transaction_id;//	Randomized by client.
	int8_t	info_hash[20];//	The info - hash of the torrent you want announce yourself in.
	int8_t	peer_id[20];//	Your peer id.
	int64_t	downloaded;//	The number of byte you've downloaded in this session.
	int64_t	left;//	The number of bytes you have left to download until you're finished.
	int64_t	uploaded;//	The number of bytes you have uploaded in this session.
	int32_t	event; 
	/*The event, one of
		none = 0
		completed = 1
		started = 2
		stopped = 3 */
	uint32_t	ip;//	Your ip address.Set to 0 if you want the tracker to use the sender of this udp packet.
	uint32_t	key;//	A unique key that is randomized by the client.
	int32_t	num_want;//	The maximum number of peers you want in the reply.Use - 1 for default.
	uint16_t	port;//	The port you're listening on.
	uint16_t	extensions;
} mxTAnnounce;

typedef struct MMXTConnectResponse {
	UINT32 transaction_id, action;
	UINT64 connection_id;
} mxTConnectResponse;

typedef struct MMXTPeerAddress {
	int32_t	ip;//	The ip of a peer in the swarm.
	uint16_t port;//	The peer's listen port.
} mxTPeerAddress;

typedef struct MMXTAnnounceResponse {
	int32_t	action; //	The action this is a reply to.Should in this case be 1 for announce.If 3 (for error) see errors.See actions.
	int32_t	transaction_id; //	Must match the transaction_id sent in the announce request.
	int32_t	interval; //	the number of seconds you should wait until reannouncing yourself.
	int32_t	leechers; //	The number of peers in the swarm that has not finished downloading.
	int32_t	seeders; //	The number of peers in the swarm that has finished downloading and are seeding.
	mxTPeerAddress* peers; // variable number of peer addresses
} mxTAnnounceResponse;

typedef struct MMXTError {
	int32_t	action;//	The action, in this case 3, for error.See actions.
	int32_t	transaction_id;//	Must match the transaction_id sent from the client.
	int8_t	error_string[1024];//	The rest of the packet is a string describing the error.
} mxTError;

mxTConnect mxt_create_connect(); 
mxTAnnounce mxt_create_announce(mxTConnect packet, mxTConnectResponse packet_resp, char hashinfo[20]);

mxTConnectResponse mxt_create_connect_resp(uint32_t *data);
mxTAnnounceResponse* mxt_create_announce_resp(int32_t *data, int32_t len);