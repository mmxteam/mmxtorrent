#include "tracker_protocol.h"

#define PEER_ADDRESS_SIZE	6
#define APR_INLINE static
//! Byte swap unsigned short
APR_INLINE apr_uint16_t swap_uint16(apr_uint16_t val)
{
	return (val << 8) | (val >> 8);
}

//! Byte swap short
APR_INLINE apr_int16_t swap_int16(apr_int16_t val)
{
	return (val << 8) | ((val >> 8) & 0xFF);
}

//! Byte swap unsigned int
APR_INLINE apr_uint32_t swap_uint32(apr_uint32_t val)
{
	val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
	return (val << 16) | (val >> 16);
}

//! Byte swap int
APR_INLINE apr_int32_t swap_int32(apr_int32_t val)
{
	val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
	return (val << 16) | ((val >> 16) & 0xFFFF);
}

APR_INLINE apr_int64_t swap_int64(apr_int64_t val)
{
	val = ((val << 8) & 0xFF00FF00FF00FF00ULL) | ((val >> 8) & 0x00FF00FF00FF00FFULL);
	val = ((val << 16) & 0xFFFF0000FFFF0000ULL) | ((val >> 16) & 0x0000FFFF0000FFFFULL);
	return (val << 32) | ((val >> 32) & 0xFFFFFFFFULL);
}

APR_INLINE apr_uint64_t swap_uint64(apr_uint64_t val)
{
	val = ((val << 8) & 0xFF00FF00FF00FF00ULL) | ((val >> 8) & 0x00FF00FF00FF00FFULL);
	val = ((val << 16) & 0xFFFF0000FFFF0000ULL) | ((val >> 16) & 0x0000FFFF0000FFFFULL);
	return (val << 32) | (val >> 32);
}

mxTConnect mxt_create_connect() {
	mxTConnect ret = { 0 };

	ret.connection_id = swap_uint64(0x41727101980);
	ret.action = 0; 
	ret.transaction_id = apr_time_now();

	return ret;
}

mxTConnectResponse mxt_create_connect_resp(uint32_t *data) {
	mxTConnectResponse ret = { 0 };

	ret.action = *data;
	ret.transaction_id = *(data+1);
	ret.connection_id = swap_uint64(*((apr_uint64_t*)data+1));

	return ret;
}

mxTAnnounce mxt_create_announce(mxTConnect packet, mxTConnectResponse packet_resp, char hashinfo[20]) {
	mxTAnnounce anc = { 0 };
	
	anc.connection_id = swap_uint64(packet_resp.connection_id);
	anc.action = swap_uint32(1);
	anc.transaction_id = packet.transaction_id;
	memcpy(anc.info_hash, hashinfo, strlen(hashinfo) + 1);
	apr_generate_random_bytes(anc.peer_id, 20);
	anc.downloaded = 0;
	anc.left = 0;
	anc.uploaded = 0;
	anc.event = 0;
	anc.ip = 0;
	anc.key = packet.transaction_id;
	anc.num_want = swap_uint32(-1);
	anc.port = swap_uint16(MMX_PORT);
	anc.extensions = 0;

	return anc;
}

mxTAnnounceResponse* mxt_create_announce_resp(int32_t *data, int32_t len) {
	mxTAnnounceResponse* resp = (mxTAnnounceResponse*)malloc(sizeof(mxTAnnounceResponse));

	resp->action = swap_int32(*data);
	resp->transaction_id = swap_int32(*(data + 1));
	resp->interval = swap_int32(*(data + 2));
	resp->leechers = swap_int32(*(data + 3));
	resp->seeders = swap_int32(*(data + 4));
	
	int num_peers = (len - 20) / 6;
	resp->peers = (mxTPeerAddress *)malloc(num_peers * PEER_ADDRESS_SIZE);
	uint8_t *ptr = data + 5;
	printf("Num of Peers %d %d\n", num_peers, PEER_ADDRESS_SIZE);
	for (int i = 0; i < num_peers; i++) {
		resp->peers[i].ip = *(int32_t*)(ptr + i * PEER_ADDRESS_SIZE);
		resp->peers[i].port = swap_uint16(*(uint16_t*)(ptr + i * PEER_ADDRESS_SIZE + 4));

		printf("We receive.. %d.%d.%d.%d:%d \n", ((uint8_t*)&resp->peers[i].ip)[0], 
			((uint8_t*)&resp->peers[i].ip)[1],
			((uint8_t*)&resp->peers[i].ip)[2],
			((uint8_t*)&resp->peers[i].ip)[3],
			resp->peers[i].port);
	}
	printf("Num of Peers %d\n", num_peers);

	return resp;
}