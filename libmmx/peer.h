#include <openssl/sha.h>

#include "utilities.h"
#include "scheduler.h"
#include "system.h"
#include "benc.h"
#include "bts.h"
#include "btypes.h"
#include "queue.h"
#include "tracker_protocol.h"

#define BT_CHOKE 0
#define BT_UNCHOKE 1
#define BT_INTERESTED 2
#define BT_NOT_INTERESTED 3
#define BT_HAVE 4
#define BT_BITFILED 5
#define BT_REQUEST 6
#define BT_PIECE 7
#define BT_CANCEL 8

BYTE* peer_handshake(BYTE *info_hash, BYTE* peer_id);
peer_choke();