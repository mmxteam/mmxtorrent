#include "peer.h"
#include <string.h>
#include "scheduler.h"


#define HANDSHAKE_SIZE 68

APR_INLINE apr_int32_t swap_int32(apr_int32_t val)
{
	val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
	return (val << 16) | ((val >> 16) & 0xFFFF);
}

BYTE* peer_handshake(BYTE *info_hash, BYTE* peer_id) {

	BYTE* handshake = (BYTE*)malloc(HANDSHAKE_SIZE);

	int idp_offset = 1;
	int reserved_offset = idp_offset + 19;
	int infohash_offset = reserved_offset + 8;
	int peer_id_offset = infohash_offset + 20;

	int prefix = swap_int32(19);
	char idp[19] = "BitTorrent protocol";
	char reserved_value[16] = "\0\0\0\0\0\0\0\0";

	handshake[0] = prefix; // length prefix of the string
	memcpy(&handshake[idp_offset], idp, sizeof(idp)); // protocol name
	printf("\n We receive.. %d %c \n", handshake[0], handshake[19]);
	memcpy(&handshake[reserved_offset], reserved_value, sizeof(reserved_value));
	memcpy(&handshake[infohash_offset], info_hash, 20);
	memcpy(&handshake[peer_id_offset], peer_id, 20);
    
    return handshake;
}

BYTE* peer_msg_keepalive() {

	BYTE* keepalive = (BYTE*)malloc(4);
	//char choke[];
	unsigned long prefix = 0;

	keepalive[0] = (prefix >> 24) & 0xFF;
	keepalive[1] = (prefix >> 16) & 0xFF;
	keepalive[2] = (prefix >> 8) & 0xFF;
	keepalive[3] = prefix & 0xFF;
	printf("To megethos tou keepalive einai: %d %d %d %d \n", keepalive[0], keepalive[1], keepalive[2], keepalive[3]);

	return keepalive;
}

BYTE* peer_msg_choke() {

	BYTE* choke = (BYTE*)malloc(4 + sizeof(uint8_t));

	unsigned long prefix = 1;

	choke[0] = (prefix >> 24) & 0xFF;
	choke[1] = (prefix >> 16) & 0xFF;
	choke[2] = (prefix >> 8) & 0xFF;
	choke[3] = prefix & 0xFF;
	printf("To megethos tou choke einai: %d %d %d %d \n", choke[0], choke[1], choke[2], choke[3]);
	uint8_t messageID = BT_CHOKE;
	choke[4] = messageID;
	printf("Choke: %d \n", choke[4]);

	return choke;
}

BYTE* peer_msg_unchoke() {

	BYTE* unchoke = (BYTE*)malloc(4 + sizeof(uint8_t));

	unsigned long prefix = 1;

	unchoke[0] = (prefix >> 24) & 0xFF;
	unchoke[1] = (prefix >> 16) & 0xFF;
	unchoke[2] = (prefix >> 8) & 0xFF;
	unchoke[3] = prefix & 0xFF;
	printf("To megethos tou unchoke einai: %d %d %d %d \n", unchoke[0], unchoke[1], unchoke[2], unchoke[3]);
	uint8_t messageID = BT_UNCHOKE;
	unchoke[4] = messageID;
	printf("Unchoke: %d \n", unchoke[4]);

	return unchoke;
}

BYTE* peer_msg_interested() {

	BYTE* interested = (BYTE*)malloc(4 + sizeof(uint8_t));

	unsigned long prefix = 1;

	interested[0] = (prefix >> 24) & 0xFF;
	interested[1] = (prefix >> 16) & 0xFF;
	interested[2] = (prefix >> 8) & 0xFF;
	interested[3] = prefix & 0xFF;
	printf("To megethos tou interested einai: %d %d %d %d \n", interested[0], interested[1], interested[2], interested[3]);
	uint8_t messageID = BT_INTERESTED;
	interested[4] = messageID;
	printf("interested: %d \n", interested[4]);

	return interested;
}

BYTE* peer_msg_not_interested() {

	BYTE* not_interested = (BYTE*)malloc(4 + sizeof(uint8_t));

	unsigned long prefix = 1;

	not_interested[0] = (prefix >> 24) & 0xFF;
	not_interested[1] = (prefix >> 16) & 0xFF;
	not_interested[2] = (prefix >> 8) & 0xFF;
	not_interested[3] = prefix & 0xFF;
	printf("To megethos tou not_interested einai: %d %d %d %d \n", not_interested[0], not_interested[1], not_interested[2], not_interested[3]);
	uint8_t messageID = BT_NOT_INTERESTED;
	not_interested[4] = messageID;
	printf("not_interested: %d \n", not_interested[4]);

	return not_interested;
}

BYTE* peer_msg_have(int piece) {

	BYTE* msghave = (BYTE*)malloc(4 + (2 * sizeof(uint8_t)));

	unsigned long prefix = 5;

	msghave[0] = (prefix >> 24) & 0xFF;
	msghave[1] = (prefix >> 16) & 0xFF;
	msghave[2] = (prefix >> 8) & 0xFF;
	msghave[3] = prefix & 0xFF;
	printf("To megethos tou msghave einai: %d %d %d %d \n", msghave[0], msghave[1], msghave[2], msghave[3]);
	uint8_t messageID = BT_HAVE;
	msghave[4] = messageID;
	printf("msghave: %d \n", msghave[4]);
	//	zero based  
//	msghave[5] = ;

	return msghave;
}

BYTE* peer_msg_bitfield(int bitfield) {

	BYTE* msgbit = (BYTE*)malloc(4 + (2 * sizeof(uint8_t)));

	unsigned long prefix = 1 + sizeof(bitfield);

	msgbit[0] = (prefix >> 24) & 0xFF;
	msgbit[1] = (prefix >> 16) & 0xFF;
	msgbit[2] = (prefix >> 8) & 0xFF;
	msgbit[3] = prefix & 0xFF;

	printf("To megethos tou bitfield einai: %d %d %d %d \n", msgbit[0], msgbit[1], msgbit[2], msgbit[3]);
	uint8_t messageID = BT_BITFILED;
	msgbit[4] = messageID;
	printf("bitfield: %d \n", msgbit[4]);
	//	uint8_t bit = bitfield;
	//	msghave[5] = ;

	return msgbit;
}

BYTE* peer_msg_request(int index, int begin, int length) {

	BYTE* msgreq = (BYTE*)malloc(4 + (2 * sizeof(uint8_t)));

	unsigned long prefix = 13;

	msgreq[0] = (prefix >> 24) & 0xFF;
	msgreq[1] = (prefix >> 16) & 0xFF;
	msgreq[2] = (prefix >> 8) & 0xFF;
	msgreq[3] = prefix & 0xFF;

	printf("To megethos tou msgreq einai: %d %d %d %d \n", msgreq[0], msgreq[1], msgreq[2], msgreq[3]);
	uint8_t messageID = BT_REQUEST;
	msgreq[4] = messageID;
	printf("msgreq: %d \n", msgreq[4]);
	//	index - begin - lenght

	return msgreq;
}