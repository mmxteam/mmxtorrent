#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include <errno.h>
#define DIE(msg) die(__FILE__, __LINE__, msg, 0)
#define SYSDIE(msg) die(__FILE__, __LINE__, msg, errno)

#if !WIN32

typedef long long _int64;
typedef unsigned long long UINT64;
typedef unsigned int UINT32;
typedef int BOOL;
typedef unsigned int UINT;
typedef unsigned char BYTE;

#endif

#define MMX_PORT 43313

char *bts_strerror(int err);
void bts_perror(int err, char *msg);
void die(char *file, int line, char *msg, int err);
int openPath(char *path, int flags);
int cacheopen(char *path, int flags, int mode);
void cacheclose(void);
void hexencode(const unsigned char *digest, int len, char *buf, int buflen);
int hexdecode(unsigned char *digest, int len, const char *buf, int buflen);

#if WIN32 || !HAVE_ON_EXIT
typedef void(*exitfn_ptr) (int, void*);
int on_exit(exitfn_ptr exitfn, void* data);
#endif

#include <apr_general.h>
#include <apr_pools.h>
#include <apr_general.h>
#include <apr_network_io.h>
#include <apr_strings.h>
#include <apr_thread_proc.h>
#include <apr_poll.h>

#if defined(APR_DECLARE_EXPORT)
#define APR_DECLARE(type)            __declspec(dllexport) type __stdcall
#define APR_DECLARE_NONSTD(type)     __declspec(dllexport) type __cdecl
#define APR_DECLARE_DATA             __declspec(dllexport)
#else
#define APR_DECLARE(type)            __declspec(dllimport) type __stdcall
#define APR_DECLARE_NONSTD(type)     __declspec(dllimport) type __cdecl
#define APR_DECLARE_DATA             __declspec(dllimport)
#endif

#ifdef _WIN32
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif  // _DEBUG
#endif  // _WIN32

#ifdef __cplusplus
extern "C" {
#endif

#define APR_INLINE static

	//! Byte swap unsigned short
	APR_INLINE apr_uint16_t swap_uint16(apr_uint16_t val);

	//! Byte swap short
	APR_INLINE apr_int16_t swap_int16(apr_int16_t val);

	//! Byte swap unsigned int
	APR_INLINE apr_uint32_t swap_uint32(apr_uint32_t val);

	//! Byte swap int
	APR_INLINE apr_int32_t swap_int32(apr_int32_t val);

	APR_INLINE apr_int64_t swap_int64(apr_int64_t val);

	APR_INLINE apr_uint64_t swap_uint64(apr_uint64_t val);

	APR_DECLARE(void) init();
	APR_DECLARE(apr_status_t) do_connect(apr_socket_t **sock, apr_pool_t *mp, char infohash[20]);
	APR_DECLARE(apr_status_t) do_client_task(apr_socket_t *sock, const char *filepath, apr_pool_t *mp);
	static void* APR_THREAD_FUNC doit(apr_thread_t *thd, void *data);

	/* default buffer size */
#define BUFSIZE			4096

	/* useful macro */
#define CRLF_STR		"\r\n"

#define MEM_ALLOC_SIZE		1024

#define DEF_SOCK_TIMEOUT	(APR_USEC_PER_SEC * 3)

#ifdef __cplusplus
}
#endif