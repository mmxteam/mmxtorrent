#include "system.h"

#ifdef _WIN32

#include <Windows.h>

int mx_get_processor_count() {
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);

	int numCPU = sysinfo.dwNumberOfProcessors;

	return numCPU;
}

#else

#include <sys/sysctl.h>
#include <stdio.h>

int mx_get_processor_count() {
    int numCPU = 0;
    if( numCPU < 1 ) {
        int mib[4];
        size_t len;
        mib[0] = CTL_HW;
        mib[1] = HW_NCPU; // alternatively, try HW_NCPU;
        sysctl(mib, 2, &numCPU, &len, NULL, 0);
        if( numCPU < 1 ) {
            mib[1] = HW_NCPU;
            sysctl( mib, 2, &numCPU, &len, NULL, 0 );
            if( numCPU < 1 ) {
                numCPU = 1;
            }
        }
    }
    if( numCPU < 1 ) numCPU = 1;
    return numCPU;
}

#endif