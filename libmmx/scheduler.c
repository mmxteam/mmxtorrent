#include <openssl/sha.h>

#include <natpmp.h>

#include "utilities.h"
#include "scheduler.h"
#include "system.h"
#include "benc.h"
#include "bts.h"
#include "job.h"
#include "btypes.h"
#include "queue.h"
#include "tracker_protocol.h"
#include "bitset.h"

#define PEER_ID_SIZE 20
#define SHA_HASH_SIZE 20
#define MMX_CHECK_SIZE 1000
#define QUEUESIZE 30
#define MAXREQUEST (64 * 1024) /* maximum request to send */
#define MAXMESSAGE (MAXREQUEST + 100) /* maximum message size we can receive */

/**
* Shared context between main-thread and sub-thread.
* In this sample, main-thread wakes sub-thread up.
*/
typedef struct {
	/* condition variable should be used with a mutex variable */
	apr_thread_mutex_t *mutex;
	apr_thread_cond_t  *cond;

	/* shared context depends on application */
	BOOL isEmpty;
} mxQueueGuard;

typedef enum MMXSchedulerState {
	MMX_UNINITED,
	MMX_INITED
} mxSchedulerState;

typedef enum MMXPeerState {
	MMX_PEER_HANDSHAKEN,
	MMX_PEER_NEW,
	MMX_PEER_BITFIELD_SENT,
	MMX_PEER_ACTIVE
} mxPeerState;

typedef struct MMXTorrentFile {
	apr_array_header_t *trackerList;
	apr_array_header_t *peerList;
	BYTE info_hash[20];

	int piece_len;
	int file_len;
	int piece_count;
	char file_name[256];
	char* file_hashes;

	kBitSet* bset;
	kBitSet* interested;
	kBitSet* requested;
} mxTorrentFile;

typedef struct MMXTrackerState {
	int port;
	char protocol[4];
	char url[128];
} mxTrackerState;

typedef struct MMXWorker{
	apr_pool_t *mem_pool;
} mxWorker;

typedef struct MMXAskTrackerBlob {
	mxTrackerState* tracker;
	mxTorrentFile* torrent;
} mxAskTrackerBlob;

typedef struct MMXPortMappingBlob {
	int external_port;
	int internal_port;
} mxPortMappingBlob;

typedef struct MMXPortMappingRespBlob {
	int		public_port;
	int		private_port;
	BYTE	ip[4];
	int		mappinglifetime;
} mxPortMappingRespBlob;

typedef struct MMXAddPeerBlob {
	mxTAnnounceResponse* resp;
	mxTorrentFile* torr;
	int num_peers;
} mxAddPeerBlob;

typedef struct MMXRequest {
	int block;
	int offset;
	int length;
} mxRequest;

typedef struct MMXRequestQueue {
	int head;
	int tail;
	mxRequest req[QUEUESIZE];
} mxRequestQueue;

typedef struct MMXPeerStatus {
	mxPeerState st;
	time_t send_time;	/* time of last unchoke */
	time_t total_time;	/* total time sending */
	unsigned int choked : 1;	/* peer isn't sending */
	unsigned int snubbed : 1;	/* no data from peer */
	unsigned int interested : 1;	/* peer wants something */
	unsigned int unreachable : 1;	/* can't connect to peer */
} mxPeerStatus;

typedef struct MMXPeer {
	/*
	* INIT - Initialized
	* CONNECT - Connected
	* GOOD - Handshake exchanged
	*/
	char id[PEER_ID_SIZE];
	apr_socket_t* sock;
	int port;
	int time;
	BYTE ip[4];
	char *error;

	mxTorrentFile* peered_file;

	mxPeerStatus peer_st;
	kBitSet* bset;
} mxPeer;

struct MMXScheduler {
	mxSchedulerState status;

	BOOL torrent_added;

	apr_pool_t *mem_pool;
	apr_thread_t *scheduler;
	apr_thread_t **workers;
	apr_threadattr_t *thd_attr;

	/* server socket */
	apr_pollset_t *pset;
	apr_socket_t *server;
	apr_sockaddr_t *server_addr;

	int		public_port;
	int		private_port;
	BYTE	ip[4];

	btStream *bts;
	QUEUE *worker_q;
	QUEUE *scheduler_q;
	apr_array_header_t *torrentList;

	mxQueueGuard scheduler_quard;
	mxQueueGuard worker_quard;

	mxWorker** worker_state;
} mxScheduler;

APR_DECLARE(int) _mxt_accept(apr_pollset_t *pollset, apr_socket_t *lsock);

void handle_peer_read(mxPeer* peer) {
	if (!peer) return;
	if (peer->peer_st.st == MMX_PEER_NEW) {
		/*
		Error checking here
		*/
	}
	else if (peer->peer_st.st == MMX_PEER_HANDSHAKEN) {
		/*
		Error checking here
		*/
	}
}

void handle_peer_write(mxPeer* peer) {
	if (!peer) return;
	if (peer->peer_st.st == MMX_PEER_NEW) {
		printf("NEW_PEER\n");
		/*
		Assign job that creates and sends handshake
		*/
		
		peer->peer_st.st = MMX_PEER_HANDSHAKEN;
	}
	else if (peer->peer_st.st == MMX_PEER_HANDSHAKEN) {
		/*
		Assign job that sends bitfields
		*/
		printf("HANDSHAKEN\n");
	}
}

/**
* Thread entry point
*/
static void* APR_THREAD_FUNC _mxt_schedule(apr_thread_t *thd, void *data)
{
	apr_status_t rv;
	apr_pool_t *mp;
	apr_int32_t num;
	const apr_pollfd_t *ret_pfd;

	printf("Scheduler : %d\n", 1);
	
	while (TRUE) {
		QUEUENODE *next = QueueGet(mxScheduler.scheduler_q);
		//printf("Thread ID : %d %d\n", next == NULL);
		if (next == NULL) {

			apr_thread_mutex_lock(mxScheduler.scheduler_quard.mutex);
			//while ((next = QueueGet(mxScheduler.worker_q)) == NULL) {
			//printf("Scheduler Waiting Signal\n");
			apr_thread_cond_timedwait(mxScheduler.scheduler_quard.cond, mxScheduler.scheduler_quard.mutex, 2000000);
			//}
			//printf("Scheduler Received Signal\n");
			apr_thread_mutex_unlock(mxScheduler.scheduler_quard.mutex);
		}

		if (next != NULL) {
			mxJob* j = (mxJob*)next->data;
			j->callback(j->data, NULL);
			printf("Got job\n");
		}
		
		rv = apr_pollset_poll(mxScheduler.pset, 100, &num, &ret_pfd);
		if (rv == APR_SUCCESS) {
			int i;
			assert(num > 0);
			for (i = 0; i < num; i++) {
				if (ret_pfd[i].desc.s == mxScheduler.server) {
					//do_accept(pollset, lsock, mp);
					apr_socket_t *ns;/* accepted socket */
					apr_status_t rv;
					_mxt_accept(mxScheduler.pset, mxScheduler.server);
				} else {
					//printf("Other\n");
					if (ret_pfd[i].rtnevents == APR_POLLIN) {
						handle_peer_read(ret_pfd[i].client_data);
					}
					else if (ret_pfd[i].rtnevents == APR_POLLOUT) {
						//printf("Writing\n");
						handle_peer_write(ret_pfd[i].client_data);
					}
				}
			}
		}
	}
	apr_thread_exit(thd, APR_SUCCESS);

	return NULL;
}

/**
* Thread entry point
*/
static void* APR_THREAD_FUNC _mxt_worker(apr_thread_t *thd, void *data)
{
	mxWorker* self = (mxWorker*)data;
	printf("Thread ID : %d %d\n", 1, self == NULL);

	while (TRUE) {
		QUEUENODE *next = QueueGet(mxScheduler.worker_q);
		//printf("Thread ID : %d %d\n", next == NULL);
		if (next == NULL) {

			apr_thread_mutex_lock(mxScheduler.worker_quard.mutex);
			while ( ( next = QueueGet(mxScheduler.worker_q) ) == NULL ) {
				printf("Waiting Signal\n");
				apr_thread_cond_wait(mxScheduler.worker_quard.cond, mxScheduler.worker_quard.mutex);
			}
			printf("Received Signal\n");
			apr_thread_mutex_unlock(mxScheduler.worker_quard.mutex);

		} 
		//printf("Is null %d\n", next == NULL);
		//printf("Is null %d\n", next->data == NULL);
		printf("Got job self is %d\n", self == NULL);
		mxJob* j = (mxJob*)next->data;
		j->callback(j->data, self);
		
		//break;
	}

	apr_thread_exit(thd, APR_SUCCESS);

	return NULL;
}

static BOOL _mxt_init_threads() {
	apr_status_t ret;

	/* 
	   1 thread for GUI
	   1 thread for scheduler
	*/
	int cpu_count = mx_get_processor_count();
	if (cpu_count <= 3)
		cpu_count = 1;
	else
		cpu_count = cpu_count - 2;

    printf("Processor count : %d\n", cpu_count);
	/* The default thread attribute: detachable */
	apr_threadattr_create(&mxScheduler.thd_attr, mxScheduler.mem_pool);

	mxScheduler.worker_state = (mxWorker **)malloc((cpu_count) * sizeof(mxWorker*));
	mxScheduler.workers = (apr_thread_t **)malloc((cpu_count) * sizeof(apr_thread_t *));
	mxScheduler.scheduler_q = QueueCreate();
	mxScheduler.worker_q = QueueCreate();

	apr_thread_mutex_create(&mxScheduler.scheduler_quard.mutex, APR_THREAD_MUTEX_UNNESTED, mxScheduler.mem_pool);
	apr_thread_cond_create(&mxScheduler.scheduler_quard.cond, mxScheduler.mem_pool);
	apr_thread_mutex_create(&mxScheduler.worker_quard.mutex, APR_THREAD_MUTEX_UNNESTED, mxScheduler.mem_pool);
	apr_thread_cond_create(&mxScheduler.worker_quard.cond, mxScheduler.mem_pool);
	
	ret = apr_thread_create(&mxScheduler.scheduler, mxScheduler.thd_attr, _mxt_schedule, NULL, mxScheduler.mem_pool);
    
	for (int i = 0; i < cpu_count; i++) {
		mxScheduler.worker_state[i] = (mxWorker*)malloc(sizeof(mxWorker));
		apr_pool_create(&mxScheduler.worker_state[i]->mem_pool, NULL);
		ret = apr_thread_create(&mxScheduler.workers[i], mxScheduler.thd_attr, _mxt_worker, mxScheduler.worker_state[i], mxScheduler.mem_pool);
	}

    return 1;
}

//static void* APR_THREAD_FUNC mxt_add_torrent(apr_thread_t *thd, void *data);

APR_DECLARE(void) mxt_schedule_job(mxJob* j) {
	QUEUENODE *job_n;
	job_n = malloc(sizeof(QUEUENODE));
	job_n->data = j;

	switch (j->id) {
	case MX_JOB_ADD_TORRENT:
		QueuePut(mxScheduler.worker_q, job_n);
		goto signal_workers;
		break;
	case MX_JOB_ASK_TRACKER: 
		QueuePut(mxScheduler.worker_q, job_n);
		goto signal_workers;
		break;
	case MX_JOB_TORRENT_PARSED:
		break;
	case MX_JOB_CONNECT_PEER:
		QueuePut(mxScheduler.worker_q, job_n);
		goto signal_workers;
		break;
	case MX_JOB_ADD_PORT_MAPPING:
		QueuePut(mxScheduler.worker_q, job_n);
		goto signal_workers;
		break;
	case MX_JOB_ADD_PEER_CONNECTION:
		QueuePut(mxScheduler.scheduler_q, job_n);
		goto signal_scheduler;
		break;
	case MX_JOB_MAPPING_ADDED:
		QueuePut(mxScheduler.scheduler_q, job_n);
		goto signal_scheduler;
		break;
	case MX_JOB_ADD_PEER:
		QueuePut(mxScheduler.scheduler_q, job_n);
		goto signal_scheduler;
		break;
	default:
		break;
	}

signal_workers:
	apr_thread_mutex_lock(mxScheduler.worker_quard.mutex);
	puts("to wake consumer-thread up");
	apr_thread_cond_signal(mxScheduler.worker_quard.cond);
	apr_thread_mutex_unlock(mxScheduler.worker_quard.mutex);

	return;
signal_scheduler:
	apr_thread_mutex_lock(mxScheduler.scheduler_quard.mutex);
	puts("to wake consumer-thread up");
	apr_thread_cond_signal(mxScheduler.scheduler_quard.cond);
	apr_thread_mutex_unlock(mxScheduler.scheduler_quard.mutex);

	return;
}

APR_DECLARE(void) mxt_add_peer_connection(void *data, void *mem_pool) {
	printf("\n\n\n\nADD PEER RE\n\n\n\n\n");
	mxPeer* peer = (mxPeer*)data;

	apr_pollfd_t pfd = { mxScheduler.mem_pool, APR_POLL_SOCKET, APR_POLLIN | APR_POLLOUT, 0, { NULL }, peer };
	pfd.desc.s = peer->sock;

	apr_pollset_add(mxScheduler.pset, &pfd);
}

APR_DECLARE(void) mxt_add_peer(void *data, void *mem_pool) {
	mxTAnnounceResponse* resp = ((mxAddPeerBlob*)data)->resp;
	mxTorrentFile* torr = ((mxAddPeerBlob*)data)->torr;
	int num_peers = ((mxAddPeerBlob*)data)->num_peers;

	printf("Seeders %d\n", resp->seeders);

	for (int i = 0; i < num_peers; i++) {
		mxPeer* new_peer = (mxPeer*)malloc(sizeof(mxPeer));
		mxTPeerAddress tracker_peer = resp->peers[i];
		BOOL found = FALSE;
		for (int j = 0; j < torr->peerList->nelts; j++) {
			mxPeer* elem = ((mxPeer**)torr->peerList->elts)[j];
			if (memcmp(elem->ip, (char*)&tracker_peer.ip, sizeof(u_long)) == 0
				&& elem->port == tracker_peer.port) {
				found = TRUE;
				break;
			}
		}

		if (!found) {
			memcpy(new_peer->ip, (char*)&tracker_peer.ip, sizeof(u_long));
			new_peer->port = tracker_peer.port;
			new_peer->bset = NULL;
			new_peer->bset = kBitSet_create(new_peer->bset, torr->piece_count);
			new_peer->peered_file = torr;
			new_peer->peer_st.st = MMX_PEER_NEW;
			*(mxPeer**)apr_array_push(torr->peerList) = new_peer;

			mxJob* newJob = mxt_connect_peer_job(new_peer);
			mxt_schedule_job(newJob);
			/*
			apr_pollfd_t pfd = { mxScheduler.mem_pool, APR_POLL_SOCKET, APR_POLLIN | APR_POLLOUT, 0, { sock }, app_ctx };
			apr_pollset_add(mxScheduler.pset, &pfd);
			*/
		}
	}

	for (int j = 0; j < torr->peerList->nelts; j++) {
		mxPeer* elem = ((mxPeer**)torr->peerList->elts)[j];
		printf("%d.%d.%d.%d : %d\n", elem->ip[0], elem->ip[1], elem->ip[2], elem->ip[3], elem->port);
	}
}

APR_DECLARE(void) mxt_connect_peer(void *data, void *mem_pool) {
	mxPeer* peer = (mxPeer*)data;
	mxWorker* self = (mxWorker*)mem_pool;

	apr_sockaddr_t *sa;
	apr_status_t rv;
	char buffer[1024];
	sprintf(buffer, "%d.%d.%d.%d", peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3]);
	rv = apr_sockaddr_info_get(&sa, buffer, APR_INET, peer->port, 0, self->mem_pool);
	if (rv != APR_SUCCESS) {
		printf("Problem getting info");
		return rv;
	}

	rv = apr_socket_create(&peer->sock, sa->family, SOCK_STREAM, APR_PROTO_TCP, self->mem_pool);
	if (rv != APR_SUCCESS) {
		printf("Problem creating socket");
		return rv;
	}
	
	/* it is a good idea to specify socket options explicitly.
	* in this case, we make a blocking socket with timeout. */
	apr_socket_opt_set(peer->sock, APR_SO_NONBLOCK, 1);
	apr_socket_timeout_set(peer->sock, 0);

	char* ip;
	apr_sockaddr_ip_get(&ip, sa);
	printf("Got from %s \n", ip);
	printf("%d.%d.%d.%d   %d\n", peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], mem_pool == NULL);

	rv = apr_socket_connect(peer->sock, sa);
	char errbuf[1024];
	if (rv != APR_SUCCESS && !APR_STATUS_IS_EINPROGRESS(rv) ) {
		printf("\n\n\n\n\nProblem connecting %s\n\n\n\n\n", apr_strerror(rv, errbuf, 1024));
		return rv;
	} 
	printf("\n\n\n\n\nConnecting\n\n\n\n\n");

	mxJob* newJob = mxt_add_peer_connection_job(peer);
	mxt_schedule_job(newJob);

	/* see the tutorial about the reason why we have to specify options again */
	//apr_socket_opt_set(s, APR_SO_NONBLOCK, 0);
	//apr_socket_timeout_set(s, DEF_SOCK_TIMEOUT);

}

APR_DECLARE(void) mxt_add_port_mapping(void *data, void *mem_pool) {
	mxPortMappingBlob* nat_pmp = (mxPortMappingBlob*)data;
	int publicport, privateport, mappinglifetime;
	u_long ip_a;
	BOOL forwarded = FALSE;
	enum { Sinit = 0, Sdelmap, Ssendpub, Srecvpub, Ssendmap, Srecvmap, Sdone, Serror = 1000 } natpmpstate = Sinit;
	natpmp_t natpmp;
	natpmpresp_t response;
	int r;
	if ((r = initnatpmp(&natpmp, 0, -1)) < 0) {
		printf("Natpmp failed to initialize\n", r);
	}
	else {
		printf("Natpmp initialized\n", r);
		natpmpstate = Ssendpub;
	}
	while (!forwarded) {
		switch (natpmpstate) {
		case Ssendpub:
			if ((r = sendpublicaddressrequest(&natpmp)) < 0) {
				printf("Send public address request failed with %d\n", r);
				natpmpstate = Serror;
			}
			else {
				printf("Public address request sent %d\n", r);
				natpmpstate = Srecvpub;
			}
			break;
		case Sdelmap:
			if (sendnewportmappingrequest(&natpmp, NATPMP_PROTOCOL_TCP, nat_pmp->internal_port, nat_pmp->external_port, 0) < 0)
				natpmpstate = Serror;
			else
				natpmpstate = Ssendmap;
			break;
		case Srecvpub:
			r = readnatpmpresponseorretry(&natpmp, &response);
			if (r < 0 && r != NATPMP_TRYAGAIN) {
				natpmpstate = Serror;
				printf("Receive error %d\n", r);
				natpmpstate = Sdelmap;
			}
			else if (r != NATPMP_TRYAGAIN) {
				printf("Public address received %d\n", r);
				ip_a = ntohl(response.pnu.publicaddress.addr.S_un.S_addr);
				unsigned char *b = (char *)&ip_a;
				printf("%d %d %d %d\n", b[0], b[1], b[2], b[3]);
				natpmpstate = Ssendmap;
			}
			else {
				printf("Mpales\n");
				natpmpstate = Ssendmap;
			}
			break;
		case Ssendmap:
			if (sendnewportmappingrequest(&natpmp, NATPMP_PROTOCOL_TCP, nat_pmp->internal_port, nat_pmp->external_port, 3600) < 0)
				natpmpstate = Serror;
			else
				natpmpstate = Srecvmap;
			break;
		case Srecvmap:
			r = readnatpmpresponseorretry(&natpmp, &response);
			if (r < 0 && r != NATPMP_TRYAGAIN)
				natpmpstate = Serror;
			else if (r != NATPMP_TRYAGAIN) {
				printf("Returned value %d\n", r);
				publicport = response.pnu.newportmapping.mappedpublicport;
				privateport = response.pnu.newportmapping.privateport;
				mappinglifetime = response.pnu.newportmapping.lifetime;

				printf("Public %d Private %d Time %d\n", publicport, privateport, mappinglifetime);
				if (privateport != nat_pmp->internal_port) {
					natpmpstate = Sdelmap;
					break;
				}
				natpmpstate = Sdone;
				forwarded = TRUE;
			}
			break;
		default:
			break;
		}
	}

	mxPortMappingRespBlob* blob= (mxPortMappingRespBlob*)malloc(sizeof(mxPortMappingRespBlob));
	blob->mappinglifetime = mappinglifetime;
	blob->private_port = privateport;
	blob->public_port = publicport;
	memcpy(blob->ip, (char*)&ip_a, sizeof(u_long));
	mxJob* new_j = mxt_get_port_mapping_job(blob);

	mxt_schedule_job(new_j);

	closenatpmp(&natpmp);
}


APR_DECLARE(void) mxt_get_port_mapping(void *data, void *mem_pool) {
	mxPortMappingRespBlob* blob = (mxPortMappingRespBlob*)data;

	mxScheduler.public_port = blob->public_port;
	mxScheduler.private_port = blob->private_port;
	memcpy(mxScheduler.ip, blob->ip, sizeof(u_long));
}

APR_DECLARE(void) mxt_ask_tracker(void *data, void *mem_pool) {
	mxAskTrackerBlob* dummy = (mxAskTrackerBlob*)data;
	mxTrackerState* tracker = dummy->tracker;
	mxTorrentFile* torrent = dummy->torrent;
	mxWorker* self = (mxWorker*)mem_pool;

	printf("Tracker 3 : %s : %d\n", tracker->url, tracker->port);
	printf("Tracker Protocol 3 : %s\n", tracker->protocol);

	apr_thread_t* thread;
	apr_sockaddr_t *sa;
	apr_socket_t *s;
	apr_status_t rv;
	char b[256];
	int len = 256;
	printf("get\n");
	//	sa->
	rv = apr_sockaddr_info_get(&sa, tracker->url, APR_INET, tracker->port, 0, self->mem_pool);
	if (rv != APR_SUCCESS) {
		printf("Could not get info %d\n", rv);
		return;
	}
	//printf("get 2\n");
	rv = apr_socket_create(&s, sa->family, SOCK_DGRAM, APR_PROTO_UDP, self->mem_pool);
	if (rv != APR_SUCCESS) {
		printf("Could not create\n");
		return;
	}

	mxTConnect packet = mxt_create_connect();
	apr_size_t packet_len = sizeof(packet);
	char b2[256];
	int len2 = sizeof(b2);
	rv = apr_socket_sendto(s, sa, 0, (const char *)&packet, (apr_size_t *)&packet_len);
	if (rv != APR_SUCCESS) {
		printf("Could not send %d %s\n", rv, apr_strerror(rv, b2, len2));
		return;
	}
	//apr_socket_timeout_set(s, DEF_SOCK_TIMEOUT);
	uint8_t buffer[1024];
	apr_size_t buffer_len = 1024;
	rv = apr_socket_recv(s, (char *)buffer, (apr_size_t*)&buffer_len);
	if (rv != APR_SUCCESS) {
		printf("Could not rcv %d %s\n", rv, apr_strerror(rv, b, len));
		return;
	}
	mxTConnectResponse packet_resp = { 0 };
	if (buffer_len != sizeof(mxTConnectResponse)) {

	} else {
		packet_resp = mxt_create_connect_resp(buffer);
		printf("Rcved %d \n", buffer_len);
		printf("Action %d \n", packet_resp.action);
		printf("Conn %d \n", packet_resp.connection_id);
		printf("Trans %d %d\n", packet_resp.transaction_id, packet.transaction_id);
	}
	mxTAnnounce sendpacket = mxt_create_announce(packet, packet_resp, torrent->info_hash);
	apr_size_t sendpacket_len = sizeof(sendpacket);
	rv = apr_socket_sendto(s, sa, 0, (const char *)&sendpacket, (apr_size_t *)&sendpacket_len);
	if (rv != APR_SUCCESS) {
		printf("Could not send %d %s\n", rv, apr_strerror(rv, b2, len2));
		return;
	}
	uint8_t sbuffer[1024];
	apr_size_t sbuffer_len = 1024;
	rv = apr_socket_recv(s, (char *)sbuffer, (apr_size_t*)&sbuffer_len);
	if (rv != APR_SUCCESS) {
		printf("Could not rcv %d %s\n", rv, apr_strerror(rv, b, len));
		return;
	}
	/*printf("Processor count %d\n", mx_get_processor_count());
	printf("WE RECEIVE %d \n", sbuffer_len);
	printf("We receive.. %d %d %d %d \n", sbuffer[20], sbuffer[21], sbuffer[22], sbuffer[23]);
	printf("We receive.. %d %d %d %d \n", sbuffer[26], sbuffer[27], sbuffer[28], sbuffer[29]);
	printf("We receive.. %d %d %d %d \n", sbuffer[32], sbuffer[33], sbuffer[34], sbuffer[35]);
	*/
	mxTAnnounceResponse* ann_resp = mxt_create_announce_resp(sbuffer, sbuffer_len);
	printf("Action %d \n", ann_resp->action);
	printf("Leachers %d \n", ann_resp->leechers);
	printf("Seeders %d \n", ann_resp->seeders);
	//printf("We receive.. %d %d %d %d \n", sbuffer[26], sbuffer[27], sbuffer[28], sbuffer[29]);
	//printf("We receive.. %d %d %d %d \n", sbuffer[32], sbuffer[33], sbuffer[34], sbuffer[35]);

	mxAddPeerBlob* blob = (mxAddPeerBlob*)malloc(sizeof(mxAddPeerBlob));
	blob->resp = ann_resp;
	blob->torr = torrent;
	blob->num_peers = (sbuffer_len - 20) / 6;
	mxJob* new_j = mxt_add_peer_job(blob);
	
	mxt_schedule_job(new_j);
}

APR_DECLARE(int) _mxt_accept(apr_pollset_t *pollset, apr_socket_t *lsock) {
	apr_socket_t *ns;/* accepted socket */
	apr_sockaddr_t *nsa;/* accepted socket address */
	apr_sockaddr_t *nsa2;/* accepted socket address */
	char *nsaip;/* accepted socket address */
	char *nsa2ip;/* accepted socket address */
	apr_status_t rv;

	rv = apr_socket_accept(&ns, lsock, mxScheduler.mem_pool);
	if (rv == APR_SUCCESS) {
		apr_socket_addr_get(&nsa, APR_LOCAL, ns);
		apr_socket_addr_get(&nsa2, APR_REMOTE, ns);
		apr_pollfd_t pfd = { mxScheduler.mem_pool, APR_POLL_SOCKET, APR_POLLIN | APR_POLLOUT, 0, { NULL }, NULL };
		pfd.desc.s = ns;
		/* at first, we expect requests, so we poll APR_POLLIN event */
		/*serv_ctx->status = SERV_RECV_REQUEST;
		serv_ctx->cb_func = recv_req_cb;
		serv_ctx->recv.is_firstline = TRUE;
		serv_ctx->mp = mp;*/

		apr_sockaddr_ip_get(&nsaip, nsa);
		apr_sockaddr_ip_get(&nsa2ip, nsa2);
		printf("Local %s Remote %s", nsaip, nsa2ip);

		/* non-blocking socket. We can't expect that @ns inherits non-blocking mode from @lsock */
		apr_socket_opt_set(ns, APR_SO_NONBLOCK, 1);
		apr_socket_timeout_set(ns, 0);

		apr_pollset_add(pollset, &pfd);
	}
	return TRUE;
}

APR_DECLARE(void) mxt_add_torrent(void *data, void *mem_pool) {
	const char *path = (const char *)data;
	mxWorker* self = (mxWorker*)mem_pool;
	btStream *infostr;
	btStream *infostr2;
	btString *announce;
	btInteger *size;
	btObject* md;
	btObject* mdName;
	struct btstrbuf strbuf;
	struct btstrbuf strbuf2;
	btString *hashdata;
	btInteger *piecelen;

	printf("File %s\n", path);
	printf("Mpmapis");

	mxScheduler.bts = bts_create_filestream(path, BTS_INPUT);
	if (benc_get_object(mxScheduler.bts, &md)) {
		printf("File open failed\n");
		//free(dl);
	}
	mxTorrentFile* torr = (mxTorrentFile*)malloc(sizeof(mxTorrentFile));
	torr->trackerList = apr_array_make(mxScheduler.mem_pool, 4, sizeof(mxTrackerState*));
	torr->peerList = apr_array_make(mxScheduler.mem_pool, 4, sizeof(mxPeer*));
	
	btList *announceList = btObject_val(md, "announce-list");
	infostr = bts_create_strstream(BTS_OUTPUT);
	benc_put_object(infostr, btObject_val(md, "announce-list"));
	strbuf = bts_get_buf(infostr);

	printf("List %s TYPEEEEEEEEE %d\n", strbuf.buf, md->t);

	btDict* fileInfo = btObject_val(md, "info");
	btString* fileName = btObject_val(fileInfo, "name");
	btInteger* fileLength = btObject_val(fileInfo, "length");
	btInteger* filePieceLength = btObject_val(fileInfo, "piece length");
	btString* filePieces = btObject_val(fileInfo, "pieces");
	printf("File Name %s Length %d Piece Len %d\n", btString_buf(fileName), fileLength->ival, filePieceLength->ival);
	printf("Pieces len %d Num Pieces %d\n", btString_len(filePieces), (int)((btString_len(filePieces) / 20.0 ) + 0.5));

	strcpy(torr->file_name, btString_buf(fileName));
	torr->file_name[btString_len(fileName)] = 0;
	torr->file_hashes = (char *)malloc(btString_len(filePieces));
	memcpy(torr->file_hashes, btString_buf(filePieces), btString_len(filePieces));
	torr->file_len = fileLength->ival;
	torr->piece_len = filePieceLength->ival;
	torr->piece_count = (int)((btString_len(filePieces) / 20.0) + 0.5);

	for (int j = 0; j < announceList->listsize; j++) {
     btList *trackers = announceList->list[j];
     for (int i = 0; i < trackers->listsize; i++) {
		 mxTrackerState* new_tracker = (mxTrackerState*)malloc(sizeof(mxTrackerState));
         char t_port[10];
         
         int len;
         int port_no;
         btObject *tracker = trackers->list[i];
         printf("Type %d\n", tracker->t);
         infostr = bts_create_strstream(BTS_OUTPUT);
         benc_put_string(infostr, tracker);
         strbuf = bts_get_buf(infostr);
         
         char* tracker_start = strstr(strbuf.buf, ":");
         int i = 0;
         tracker_start++;
         while (*tracker_start != ':') {
			 new_tracker->protocol[i++] = *tracker_start++;
         }
		 new_tracker->protocol[i] = 0;
		 printf("Tracker Protocol : %s\n", new_tracker->protocol);
		 i = 0;
         tracker_start++;
         tracker_start++;
         tracker_start++;
         while (*tracker_start != ':') {
			 new_tracker->url[i++] = *tracker_start++;
         }
		 new_tracker->url[i] = 0;
         
         i = 0;
         tracker_start++;
         while (*tracker_start != '/') {
			 t_port[i++] = *tracker_start++;
         } 
		 t_port[i] = 0;
		 sscanf(t_port, "%d", &new_tracker->port);
         
		 *(mxTrackerState**)apr_array_push(torr->trackerList) = new_tracker;
	 }
	}

	*(mxTorrentFile**)apr_array_push(mxScheduler.torrentList) = torr;
	for (int i = 0; i < mxScheduler.torrentList->nelts; i++) {
		mxTorrentFile* elem = ((mxTorrentFile**)mxScheduler.torrentList->elts)[i];

		printf("Tracker : %s : %d\n", (*(mxTrackerState**)&elem->trackerList->elts[0])->url, (*(mxTrackerState**)&elem->trackerList->elts[0])->port);
		printf("Tracker Protocol : %s\n", (*(mxTrackerState**)&elem->trackerList->elts[0])->protocol);
	}

	btObject *infoObj = btObject_val(md, "info");
	infostr = bts_create_strstream(BTS_OUTPUT);
	benc_put_object(infostr, btObject_val(md, "info"));
	strbuf = bts_get_buf(infostr);

	SHA1(strbuf.buf, strbuf.len, torr->info_hash);
	printf("SHA1 Hash: ");
	for (int i = 0; i < 20; i++) {
		printf("%02x ", torr->info_hash[i]);
	}

	char nameBuffer[256];
	apr_status_t rv;
	apr_file_t *fp;
	apr_file_t *fp2;
	sprintf(nameBuffer, "./MMXDownloads/%s", torr->file_name);
	printf("Torrent File : %s", nameBuffer);
	torr->bset = NULL;
	torr->bset = kBitSet_create(torr->bset, torr->piece_count);
	torr->interested = NULL;
	torr->interested = kBitSet_create(torr->interested, torr->piece_count);
	torr->requested = NULL;
	torr->requested = kBitSet_create(torr->requested, torr->piece_count);
	assert(torr->bset != NULL);
	//apr_file_remove(nameBuffer, self->mem_pool);
	if ((rv = apr_file_open(&fp, nameBuffer, APR_READ, APR_OS_DEFAULT, self->mem_pool)) != APR_SUCCESS) {
		printf("File not found");
		
		if ((rv = apr_file_open(&fp2, nameBuffer, APR_WRITE | APR_CREATE, APR_OS_DEFAULT, self->mem_pool)) != APR_SUCCESS) {
			printf("File not opened for write");
		}
		else {
			int write_len = torr->file_len;
			BYTE* dummyBytes = (BYTE*)malloc(torr->file_len);
			memset(dummyBytes, 0, torr->file_len);
			apr_file_write(fp2, dummyBytes, &write_len);

			apr_file_close(fp2);
		}
	}
	else {
		BYTE* checkBytes = (BYTE*)malloc(MMX_CHECK_SIZE * torr->piece_len);
		int max_read = MMX_CHECK_SIZE * torr->piece_len;
		while (max_read == (MMX_CHECK_SIZE * torr->piece_len)) {
			apr_file_read(fp, checkBytes, &max_read);
			printf("Read %d Count %d %f %d", max_read, max_read / torr->piece_len, (float)max_read / torr->piece_len, ( max_read + (torr->piece_len-1) ) / torr->piece_len);
		}
		int max_piece = (max_read + (torr->piece_len - 1)) / torr->piece_len;
		for (int i = 0; i < max_piece; i++) {
			BYTE temp_sha[20];
			int next_p_idx = i * SHA_HASH_SIZE;
			int next_b_idx = i * torr->piece_len;
			BYTE* next_sha = &torr->file_hashes[next_p_idx];
			BYTE* next_block = &checkBytes[next_b_idx];
			int max_hash;
			if ( ( torr->file_len - next_b_idx ) >= torr->piece_len) {
				max_hash = torr->piece_len;
			} else {
				max_hash = torr->file_len - next_b_idx;
			}
			SHA1(next_block, max_hash, temp_sha);
			printf("SHA1 of Chunk id %d : ", i+1);
			for (int j = 0; j < 20; j++) {
				printf("%02x ", temp_sha[j]);
			}
			printf("\nSHA1 of Chunk id %d : ", i+1);
			for (int j = 0; j < 20; j++) {
				printf("%02x ", next_sha[j]);
			}
			if (memcmp(temp_sha, next_sha, 20) == 0) {
				bs_set(torr->bset, i);
			}
			printf("\n");
		}
		printf("Set %d", bs_countBits(torr->bset));
		apr_file_close(fp);
		free(checkBytes);
	}

	for (int j = 0; j < torr->trackerList->nelts; j++) {
		mxTrackerState* elem = ((mxTrackerState**)torr->trackerList->elts)[j];
		printf("Tracker 2 : %s : %d\n", elem->url, elem->port);
		printf("Tracker Protocol 2 : %s\n", elem->protocol);
		mxAskTrackerBlob* blob = (mxAskTrackerBlob*)malloc(sizeof(mxAskTrackerBlob));
		blob->torrent = torr;
		blob->tracker = elem;
		mxJob* new_j = (mxJob*)mxt_ask_tracker_job(blob);
		mxt_schedule_job(new_j);
	}
	
	/*mxJob* new_j = mxt_torrent_parsed_job();
	mxt_schedule_job(new_j);
	if (mxScheduler.status != MMX_UNINITED) {

	}*/

	peer_msg_choke();
}

APR_DECLARE(void) mxt_torrent_parsed(void *data, void *mem_pool){


}


APR_DECLARE(BOOL) mxt_initialized() {
	if ( mxScheduler.status == MMX_UNINITED )
		return FALSE;
	else
		return TRUE;

	/*
	int cpu_count = mx_get_processor_count();
	mxScheduler.workers = (apr_thread_t **)malloc(cpu_count * sizeof(apr_thread_t *));
	*/
}

static apr_socket_t* _mxt_create_listen_sock()
{
	apr_status_t rv;

	rv = apr_sockaddr_info_get(&mxScheduler.server_addr, NULL, APR_INET, MMX_PORT, 0, mxScheduler.mem_pool);
	if (rv != APR_SUCCESS) {
		goto error;
	}

	rv = apr_socket_create(&mxScheduler.server, mxScheduler.server_addr->family, SOCK_STREAM, APR_PROTO_TCP, mxScheduler.mem_pool);
	if (rv != APR_SUCCESS) {
		goto error;
	}

	/* non-blocking socket */
	apr_socket_opt_set(mxScheduler.server, APR_SO_NONBLOCK, 1);
	apr_socket_timeout_set(mxScheduler.server, 0);
	apr_socket_opt_set(mxScheduler.server, APR_SO_REUSEADDR, 1);/* this is useful for a server(socket listening) process */
	printf("hlahla");
	rv = apr_socket_bind(mxScheduler.server, mxScheduler.server_addr);
	if (rv != APR_SUCCESS) {
		goto error;
	}
	rv = apr_socket_listen(mxScheduler.server, 100);
	if (rv != APR_SUCCESS) {
		goto error;
	}

	return mxScheduler.server;

error:
	return NULL;
}

APR_DECLARE(void) mxt_initialize() {
	apr_time_t t,ts, te;
	t = apr_time_now();
	printf("The current time: %" APR_TIME_T_FMT "[us]\n", t);
	printf("The current time: %" APR_TIME_T_FMT "[ms]\n", apr_time_as_msec(t));
	printf("The current time: %" APR_TIME_T_FMT "[ms]\n", apr_time_msec(t));
	printf("The current time: %" APR_TIME_T_FMT "[s]\n", apr_time_sec(t));
	ts = apr_time_now();
	//Sleep(1000);
	te = apr_time_now();
	printf("The current time: %" APR_TIME_T_FMT "[ms]\n", apr_time_as_msec(te) - apr_time_as_msec(ts));

	mxScheduler.status = MMX_UNINITED;
	mxScheduler.torrent_added = FALSE;
	apr_status_t rv;
	apr_initialize();
	apr_pool_create(&mxScheduler.mem_pool, NULL);
	mxScheduler.torrentList = apr_array_make(mxScheduler.mem_pool, 1, sizeof(mxTorrentFile));
	_mxt_init_threads();
	if (_mxt_create_listen_sock() == NULL)
		printf("Mpoulous\n");

	apr_dir_make("./MMXDownloads", APR_DIR, mxScheduler.mem_pool);

	apr_status_t ret = apr_pollset_create(&mxScheduler.pset, 100, mxScheduler.mem_pool, APR_POLLSET_EPOLL);
	if (ret == APR_ENOTIMPL) {
		printf("epoll not supported\n");
	}	
	apr_pollfd_t pfd = { mxScheduler.mem_pool, APR_POLL_SOCKET, APR_POLLIN, 0, { NULL }, NULL };
	pfd.desc.s = mxScheduler.server;
	apr_pollset_add(mxScheduler.pset, &pfd);
	
	/*rv = apr_sockaddr_info_get(&mxScheduler.server_addr, NULL, APR_INET, MMX_PORT, 0, mxScheduler.mem_pool);
	if (rv != APR_SUCCESS) {
		printf("sockaddr error\n");
	}
	else {
		printf("sockaddr success\n");
	}

	rv = apr_socket_create(&mxScheduler.server, mxScheduler.server_addr->family, SOCK_STREAM, APR_PROTO_TCP, mxScheduler.mem_pool);
	if (rv != APR_SUCCESS) {
		printf("sock error\n");
	}
	else {
		printf("sock success\n");
	}*/

	mxPortMappingBlob* blob = (mxPortMappingBlob*)malloc(sizeof(mxPortMappingBlob));
	blob->external_port = MMX_PORT;
	blob->internal_port = MMX_PORT;
	mxJob* new_j = mxt_add_port_mapping_job(blob);
	//Sleep(100);

	mxt_schedule_job(new_j);
	/*
	int cpu_count = mx_get_processor_count();
	mxScheduler.workers = (apr_thread_t **)malloc(cpu_count * sizeof(apr_thread_t *));
	*/
}

APR_DECLARE(void) mxt_deinitialize() {
	apr_terminate();
}