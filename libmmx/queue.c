#include "utilities.h"
#include "queue.h"

QUEUE *QueueCreate() {
	QUEUE *pNewQueue;

	pNewQueue = malloc(sizeof(QUEUE));
	pNewQueue->pHead = pNewQueue->pTail = (QUEUENODE *)&pNewQueue->Dummy;
	pNewQueue->Dummy.pNext = (QUEUENODE *)pNewQueue;

	return pNewQueue;
}

void QueuePutNode(QUEUE *pQueue, QUEUENODE *pNode)
{
	volatile QUEUENODE *pTail;
	//printf("Inerting %d ", pNode->JOB_ID);
	/* Set next ptr of node to point to list header (this is null) */
	pNode->pNext = (QUEUENODE *)pQueue;
	//printf("entered 1\n");
	for (;;)                    /* Keep trying until successful */
	{
		//printf("entered\n");
		pTail = pQueue->pTail;  /* Read tail ptr */
		if (CAS(&pTail->pNext, pQueue, pNode)) /* Try to link in new node */
		{
			/* New node linked in, update tail (maybe not needed?) */
			CAS(&pQueue->pTail, pTail, pNode);
			//printf("IFed\n");
			return;
		}
		//printf("entered end\n");
		/* Try to move tail ptr & try again */
		CAS(&pQueue->pTail, pTail, pTail->pNext);
	}
}

void QueuePut(IQueue *pThis, QUEUENODE *pNode)
{
	QUEUE *pQueue;
	pQueue = pThis;
	//printf("entered\n");
	QueuePutNode(pQueue, pNode);
	/*if (pQueue->pCond)
		Condition_Signal(pQueue->pCond);*/

	return;
}

/* Get a node from the head of the queue.
* This does double-duty: it's used for both Get and GetFromInterrupt
*/
QUEUENODE *QueueGet(IQueue *pThis)
{
	QUEUENODE *pHead;
	QUEUENODE *pTail;
	QUEUENODE *pNext;
	QUEUE *pQueue;

	pQueue = pThis;

	for (;;)
	{
		pHead = pQueue->pHead;  /* Read head ptr */
		pTail = pQueue->pTail;  /* Read tail ptr */
		pNext = pHead->pNext;   /* Read head->next ptr */
		if (pHead != pQueue->pHead) /* validate pHead */
		{
			continue;           /* invalid - try again */
		}

		if (pHead == pTail)     /* Is queue empty or tail failing behind */
		{
			if (pNext == (QUEUENODE *)pQueue) /* Is queue empty? */
			{
				return NULL;    /* Yes, nothing to return */
			}
			/* Tail falling behind, try to advance */
			CAS(&pQueue->pTail, pTail, pNext);
			continue;           /* and try again */
		}

		if (CAS(&pQueue->pHead, pHead, pNext)) /* Try to swing head to next node */
		{                       /* pHead is now ours! */
			if (pHead != (QUEUENODE *)&pQueue->Dummy) /* Is it a 'real' node? */
			{
				return pHead;   /* Yes- return it */
			}
			/* It's the dummy. Queue it back up & try again */
			QueuePutNode(pQueue, pHead);
		}
	}
}