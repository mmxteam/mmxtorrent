#ifndef __MMX_JOB__H
#define __MMX_JOB__H

#include "utilities.h"

typedef enum MMXJobID {
	MX_JOB_ADD_TORRENT,
	MX_JOB_TORRENT_PARSED,
	MX_JOB_PEERS_ADDED,
	MX_JOB_ASK_TRACKER,
	MX_JOB_ADD_PORT_MAPPING,
	MX_JOB_MAPPING_ADDED,
	MX_JOB_HANDSHAKE_PEER,
	MX_JOB_CONNECT_PEER,
	MX_JOB_ADD_PEER_CONNECTION,
	MX_JOB_ADD_PEER
} mxJobID;

typedef struct MMXJob {
	mxJobID id;
	void* data;
	apr_time_t last;
	apr_time_t left;
	void (APR_THREAD_FUNC *callback)(void*, void*);
} mxJob;

typedef struct MMXTrackerState mxTrackerState;
typedef struct MMXTorrentFile mxTorrentFile;

typedef struct MMXAskTrackerBlob mxAskTrackerBlob;
typedef struct MMXPortMappingBlob mxPortMappingBlob;
typedef struct MMXPortMappingRespBlob mxPortMappingRespBlob;
typedef struct MMXAddPeerBlob mxAddPeerBlob;
typedef struct MMXPeer mxPeer;

#ifdef __cplusplus
extern "C" {
#endif

	APR_DECLARE(mxJob*) mxt_add_torrent_job(const char *path);
	APR_DECLARE(mxJob*) mxt_ask_tracker_job(mxAskTrackerBlob* tracker);
	APR_DECLARE(mxJob*) mxt_add_peer_job(mxAddPeerBlob* peer);
	APR_DECLARE(mxJob*) mxt_connect_peer_job(mxPeer* peer);
	APR_DECLARE(mxJob*) mxt_add_peer_connection_job(mxPeer* peer);
	APR_DECLARE(mxJob*) mxt_add_port_mapping_job(mxPortMappingBlob* pmap);
	APR_DECLARE(mxJob*) mxt_get_port_mapping_job(mxPortMappingRespBlob* pmap);
	APR_DECLARE(mxJob*) mxt_torrent_parsed_job(mxTorrentFile* torrent);

#ifdef __cplusplus
}
#endif

#endif