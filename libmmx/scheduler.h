#ifndef __MMX_SCHEDULER__H
#define __MMX_SCHEDULER__H

#include "job.h"

#ifdef __cplusplus
extern "C" {
#endif

APR_DECLARE(void) mxt_schedule_job(mxJob* j);
APR_DECLARE(BOOL) mxt_initialized();
APR_DECLARE(void) mxt_initialize();
APR_DECLARE(void) mxt_deinitialize();

APR_DECLARE(void) mxt_add_torrent(void *data, void *mem_pool);
APR_DECLARE(void) mxt_ask_tracker(void *data, void *mem_pool);
APR_DECLARE(void) mxt_add_peer(void *data, void *mem_pool);
APR_DECLARE(void) mxt_connect_peer(void *data, void *mem_pool);
APR_DECLARE(void) mxt_add_peer_connection(void *data, void *mem_pool);
APR_DECLARE(void) mxt_add_port_mapping(void *data, void *mem_pool);
APR_DECLARE(void) mxt_get_port_mapping(void *data, void *mem_pool);
APR_DECLARE(void) mxt_handshake_peer(void *data, void *mem_pool);
APR_DECLARE(void) mxt_torrent_parsed(void *data, void *mem_pool);

//typedef int(*socket_callback_t)(mxScheduler* serv_ctx, apr_pollset_t *pollset, apr_socket_t *sock);
#ifdef __cplusplus
}
#endif

#endif // __MMX_SCHEDULER__H