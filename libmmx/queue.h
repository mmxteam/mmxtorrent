#include <apr_general.h>
#include <apr_thread_proc.h>
#include <apr_thread_cond.h>
#include <apr_atomic.h>

typedef struct _QUEUENODE QUEUENODE;

typedef struct _QUEUENODE
{
	QUEUENODE *pNext;
	void* data;
} QUEUENODE;

typedef struct _MINQUEUENODE
{
	QUEUENODE *pNext;
} MINQUEUENODE;

typedef struct _QUEUE
{
	const struct IQueueVtbl *Vtbl;
	UINT RefCount;
	QUEUENODE *volatile pHead;
	QUEUENODE *volatile pTail;
	MINQUEUENODE Dummy; /* Should be 'QUEUENODE Dummy', but we only need first field */
	
} QUEUE, IQueue, *PQUEUE;

//#define CAS(_ptr_,_old_,_new_) AtomicCmpAndSwap((PUINT)(_ptr_),(UINT)(_old_),(UINT)(_new_))
#define CAS(_ptr_,_old_,_new_) (apr_atomic_casptr((_ptr_),(_new_),(_old_)) != *(_ptr_))

QUEUE *QueueCreate();
void QueuePutNode(QUEUE *pQueue, QUEUENODE *pNode);
void QueuePut(IQueue *pThis, QUEUENODE *pNode);
QUEUENODE *QueueGet(IQueue *pThis);