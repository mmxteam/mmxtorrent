#include "job.h"

#include "utilities.h"
#include "scheduler.h"

APR_DECLARE(mxJob*) mxt_add_torrent_job(const char *path) {
	mxJob* new_job = (mxJob*)malloc(sizeof(mxJob));

	new_job->id = MX_JOB_ADD_TORRENT;

	new_job->data = malloc(strlen(path));
	strcpy(new_job->data, path);

	new_job->callback = mxt_add_torrent;

	return new_job;
}

APR_DECLARE(mxJob*) mxt_ask_tracker_job(mxAskTrackerBlob* data) {
	mxJob* new_job = (mxJob*)malloc(sizeof(mxJob));

	new_job->id = MX_JOB_ASK_TRACKER;

	new_job->data = data;
	new_job->callback = mxt_ask_tracker;

	return new_job;
}

APR_DECLARE(mxJob*) mxt_add_peer_job(mxAddPeerBlob* peer) {
	mxJob* new_job = (mxJob*)malloc(sizeof(mxJob));

	new_job->id = MX_JOB_ADD_PEER;

	new_job->data = peer;
	new_job->callback = mxt_add_peer;

	return new_job;
}

APR_DECLARE(mxJob*) mxt_connect_peer_job(mxPeer* peer) {
	mxJob* new_job = (mxJob*)malloc(sizeof(mxJob));

	new_job->id = MX_JOB_CONNECT_PEER;

	new_job->data = peer;
	new_job->callback = mxt_connect_peer;

	return new_job;
}

APR_DECLARE(mxJob*) mxt_add_peer_connection_job(mxPeer* peer) {
	mxJob* new_job = (mxJob*)malloc(sizeof(mxJob));

	new_job->id = MX_JOB_ADD_PEER_CONNECTION;

	new_job->data = peer;
	new_job->callback = mxt_add_peer_connection;

	return new_job;
}

APR_DECLARE(mxJob*) mxt_torrent_parsed_job(mxTorrentFile* torrent) {
	mxJob* new_job = (mxJob*)malloc(sizeof(mxJob));

	new_job->id = MX_JOB_TORRENT_PARSED;

	new_job->data = torrent;
	new_job->callback = mxt_torrent_parsed;

	return new_job;
}

APR_DECLARE(mxJob*) mxt_add_port_mapping_job(mxPortMappingBlob* pmap) {
	mxJob* new_job = (mxJob*)malloc(sizeof(mxJob));

	new_job->id = MX_JOB_ADD_PORT_MAPPING;

	new_job->data = pmap;
	new_job->callback = mxt_add_port_mapping;

	return new_job;
}

APR_DECLARE(mxJob*) mxt_get_port_mapping_job(mxPortMappingRespBlob* pmap) {
	mxJob* new_job = (mxJob*)malloc(sizeof(mxJob));

	new_job->id = MX_JOB_MAPPING_ADDED;

	new_job->data = pmap;
	new_job->callback = mxt_get_port_mapping;

	return new_job;
}



