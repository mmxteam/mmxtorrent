//
//  main.m
//  MMX
//
//  Created by Nick Vitsas on 12/26/14.
//  Copyright (c) 2014 vikokat. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
