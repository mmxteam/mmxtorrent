//
//  AppDelegate.h
//  MMX
//
//  Created by Nick Vitsas on 12/26/14.
//  Copyright (c) 2014 vikokat. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

