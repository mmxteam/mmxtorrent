// wxWidgets "Hello world" Program
// For compilers that support precompilation, includes "wx/wx.h".

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#ifdef _WIN32
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif  // _DEBUG
#endif  // _WIN32

void SetStdOutToNewConsole()
{
#ifdef _WIN32
	// allocate a console for this app
	AllocConsole();

	// redirect unbuffered STDOUT to the console
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	int fileDescriptor = _open_osfhandle((intptr_t)consoleHandle, _O_TEXT);
	FILE *fp = _fdopen(fileDescriptor, "w");
	*stdout = *fp;
	setvbuf(stdout, NULL, _IONBF, 0);

	consoleHandle = GetStdHandle(STD_ERROR_HANDLE);
	fileDescriptor = _open_osfhandle((intptr_t)consoleHandle, _O_TEXT);
	fp = _fdopen(fileDescriptor, "w");
	*stderr = *fp;

	setvbuf(stderr, NULL, _IONBF, 0);

	SetConsoleTitle(L"Debug Output");

	// give the console window a bigger buffer size
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	if (GetConsoleScreenBufferInfo(consoleHandle, &csbi))
	{
		COORD bufferSize;
		bufferSize.X = csbi.dwSize.X;
		bufferSize.Y = 30000;
		SetConsoleScreenBufferSize(consoleHandle, bufferSize);
	}

#endif
}

#include <wx/sstream.h>
#include <wx/protocol/http.h>
#include <wx/filename.h>

#include <utilities.h>
#include <scheduler.h>

class MMXApp : public wxApp
{
public:
	virtual bool OnInit();
};
class MMXFrame : public wxFrame
{
public:
	MMXFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
private:
	void OnHello(wxCommandEvent& event);
	void OnExit(wxCommandEvent& event);
	void OnAbout(wxCommandEvent& event);
	wxDECLARE_EVENT_TABLE();
};
enum
{
	ID_Hello = 1
};
wxBEGIN_EVENT_TABLE(MMXFrame, wxFrame)
EVT_MENU(ID_Hello, MMXFrame::OnHello)
EVT_MENU(wxID_EXIT, MMXFrame::OnExit)
EVT_MENU(wxID_ABOUT, MMXFrame::OnAbout)
wxEND_EVENT_TABLE()

wxIMPLEMENT_APP(MMXApp);

bool MMXApp::OnInit()
{
	SetStdOutToNewConsole();

	//init();
	mxt_initialize();
	MMXFrame *frame = new MMXFrame("MMX Torrent", wxPoint(50, 50), wxSize(450, 340));
	frame->Show(true);
	return true;
}

MMXFrame::MMXFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
	: wxFrame(NULL, wxID_ANY, title, pos, size)
{
	wxMenu *menuFile = new wxMenu;
	menuFile->Append(ID_Hello, "&Hello...\tCtrl-H",
		"Help string shown in status bar for this menu item");
	menuFile->AppendSeparator();
	menuFile->Append(wxID_EXIT);
	wxMenu *menuHelp = new wxMenu;
	menuHelp->Append(wxID_ABOUT);
	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append(menuFile, "&File");
	menuBar->Append(menuHelp, "&Help");
	SetMenuBar(menuBar);
	CreateStatusBar();
	SetStatusText("Welcome to wxWidgets!");
}

/*
union {
#ifdef LITTLE_ENDIAN
	apr_uint16_t s_lo, s_hi;
#else
	apr_uint16_t s_hi, s_lo;
#endif
	apr_uint32_t l;
};*/

#define Swap8Bytes(val) \
	 ( (((val) >> 56) & 0x00000000000000FF) | (((val) >> 40) & 0x000000000000FF00) | \
	   (((val) >> 24) & 0x0000000000FF0000) | (((val) >>  8) & 0x00000000FF000000) | \
	   (((val) <<  8) & 0x000000FF00000000) | (((val) << 24) & 0x0000FF0000000000) | \
	   (((val) << 40) & 0x00FF000000000000) | (((val) << 56) & 0xFF00000000000000) )

/**
* Send a request as a simple HTTP request protocol.
* Write the received response to the standard output until the EOF.
*/

void MMXFrame::OnExit(wxCommandEvent& event)
{
	Close(true);
}
void MMXFrame::OnAbout(wxCommandEvent& event)
{
	wxMessageBox("This is a wxWidgets' Hello world sample",
		"About Hello World", wxOK | wxICON_INFORMATION);
}

static const wxChar *FILETYPES = _T("Torrent files|*.torrent|")
_T("MP4 files|*.mp4|")
_T("AVI files|*.avi|")
_T("All files|*.*");
void MMXFrame::OnHello(wxCommandEvent& event)
{
	wxFileDialog* openFileDialog =
		new wxFileDialog(this, _("Open File"), "", "", FILETYPES, wxFD_OPEN, wxDefaultPosition);

	if (openFileDialog->ShowModal() == wxID_OK)
	{
		wxString path;
		path.append(openFileDialog->GetDirectory());
		path.append(wxFileName::GetPathSeparator());
		path.append(openFileDialog->GetFilename());
		wxString str = path;
		wxPrintf("Path %s", str);
		mxJob* new_j = mxt_add_torrent_job(str.c_str());
		mxt_schedule_job(new_j);
	}

	delete openFileDialog;
	/*
	wxHTTP trackerConnection;
	trackerConnection.SetTimeout(5);
	trackerConnection.SetHeader(_T("Content-type"), _T("text/html; charset=utf-8"));
	while (!trackerConnection.Connect(_T("www.google.com")))  // only the server, no pages here yet ...
		wxSleep(5);

	wxInputStream *httpStream = trackerConnection.GetInputStream(_T("/intl/en/about.html"));

	wxString res;
	wxStringOutputStream out_stream(&res);
	httpStream->Read(out_stream);

	wxMessageBox(res);

	if (trackerConnection.IsConnected())
		wxLogMessage("Hello world from wxWidgets!");
	trackerConnection.SetHeader(_T("peer_id"), _T("kotsonis"));
	trackerConnection.SetHeader(_T("uploaded"), _T("300"));
	wxLogMessage("Hello world from wxWidgets! " + trackerConnection.GetHeader(_T("uploaded")));
*/
}